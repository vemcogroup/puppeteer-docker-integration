<?php

namespace Vemcogroup\Puppeteer;

use Symfony\Component\Process\Process;
use Vemcogroup\Puppeteer\Exceptions\PuppeteerException;

class Puppeteer
{
    private $network = '';
    private $shmSize = '1G';
    private $screenshotPath = '/tmp/screenshots';
    private $puppeteerVersion = 'latest';
    private $canvas = 'https://example.com/';
    private $resolution = '1366x768';
    private $volumes = [];
    private $screenshotType = 'screenshot';
    private $sudo = false;
    private $delay = 0;

    public function getShellCommand()
    {
        $volumes = array_reduce(array_keys($this->volumes), function ($result, $sourcePath) {
            return $result . '-v ' . escapeshellarg($sourcePath) . ':' . escapeshellarg($this->volumes[$sourcePath]) . ' ';
        }, '');

        $prefix = '';

        if ($this->sudo) {
            $prefix .= 'sudo ';
        }

        return $prefix . 'docker run --shm-size '
            . $this->shmSize 
            . $this->network
            . ' --rm -v '
            . escapeshellarg($this->screenshotPath)
            . ':/screenshots ' . $volumes . 'alekzonder/puppeteer:'
            . $this->puppeteerVersion
            . ' ' . $this->screenshotType
            . ' ' . escapeshellarg($this->canvas)
            . ' ' . $this->resolution
            . ' ' . $this->delay
            ;
    }

    public function setShmSize($shmSize): Puppeteer
    {
        $this->shmSize = $shmSize;

        return $this;
    }
    
    public function setNetwork($network): Puppeteer
    {
        $this->network = ' --network ' . $network;

        return $this;
    }

    public function setScreenshotPath($path): Puppeteer
    {
        $this->screenshotPath = $path;

        return $this;
    }

    public function setPuppeteerVersion($version): Puppeteer
    {
        $this->puppeteerVersion = $version;

        return $this;
    }

    public function setCanvas($canvas): Puppeteer
    {
        $this->canvas = $canvas;

        return $this;
    }

    public function setResolution($resolution): Puppeteer
    {
        $this->resolution = $resolution;

        return $this;
    }

    public function setScreenshotType($type): Puppeteer
    {
        $this->screenshotType = $type;

        return $this;
    }

    public function addVolume($sourcePath, $destinationPath): Puppeteer
    {
        $this->volumes[$sourcePath] = $destinationPath;

        return $this;
    }

    public function sudo(): Puppeteer
    {
        $this->sudo = true;

        return $this;
    }

    public function setDelay(int $delayInMilliseconds): Puppeteer
    {
        $this->delay = $delayInMilliseconds;

        return $this;
    }

    public function shoot(): string
    {
        $process = Process::fromShellCommandline($this->getShellCommand());
        $process->run();

        if (! $process->isSuccessful()) {
            throw new PuppeteerException($process->getErrorOutput());
        }

        $result = json_decode($process->getOutput());

        return $result->filename;
    }
}
