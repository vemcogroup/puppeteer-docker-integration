# Puppeteer docker integration

## Description

Interact with puppeteer within alekzonder/puppeteer's docker image

## Installation

Via composer
```bash
composer require vemcogroup/puppeteer-docker-integration
```

## Usage

```php
(new Puppeteer)
    ->setScreenshotPath('/my-screenshots')
    ->setCanvas('https://example.com')
    ->setResolution('1920x1080')
    ->setScreenshotType('full_screenshot_series')
    ->shoot();
```

Your screenshot should now be available in /my-screenshots
