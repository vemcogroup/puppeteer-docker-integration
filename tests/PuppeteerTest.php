<?php

use PHPUnit\Framework\TestCase;
use Vemcogroup\Puppeteer\Puppeteer;
use Vemcogroup\Puppeteer\Exceptions\PuppeteerException;

class PuppeteerTest extends TestCase
{
    /**
     * @var Puppeteer
     */
    protected $puppeteer;

    protected function setUp(): void
    {
        parent::setUp();

        $this->puppeteer = new Puppeteer();
    }

    /**
     * @test
     */
    public function check_that_a_string_is_returned()
    {
        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_shm_size()
    {
        $this->puppeteer->setShmSize('512M');

        $this->assertEquals('docker run --shm-size 512M --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_screenshots_path()
    {
        $this->puppeteer->setScreenshotPath('/my/path');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/my/path\':/screenshots alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_puppeteer_version()
    {
        $this->puppeteer->setPuppeteerVersion('1.0.0');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:1.0.0 screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_canvas_to_screenshot()
    {
        $this->puppeteer->setCanvas('file:///tmp/helloworld.html');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest screenshot \'file:///tmp/helloworld.html\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_resolution()
    {
        $this->puppeteer->setResolution('1920x1080');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1920x1080 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function set_screenshot_type()
    {
        $this->puppeteer->setScreenshotType('full_screenshot');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest full_screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function add_a_volume()
    {
        $this->puppeteer->addVolume('/my/path', '/their/path');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots -v \'/my/path\':\'/their/path\' alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function add_volumes()
    {
        $this->puppeteer
            ->addVolume('/my/path', '/their/path')
            ->addVolume('/my/second/path', '/their/second/path');

        $this->assertEquals('docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots -v \'/my/path\':\'/their/path\' -v \'/my/second/path\':\'/their/second/path\' alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function run_as_sudo()
    {
        $this->puppeteer->sudo();

        $this->assertEquals('sudo docker run --shm-size 1G --rm -v \'/tmp/screenshots\':/screenshots alekzonder/puppeteer:latest screenshot \'https://example.com/\' 1366x768 0', $this->puppeteer->getShellCommand());
    }

    /**
     * @test
     */
    public function is_a_file_produced()
    {
        if (! file_exists('/screenshots')) {
            mkdir('/screenshots');
        }

        $filename = $this->puppeteer
            ->setScreenshotPath('/screenshots')
            ->setScreenshotType('full_screenshot_series')
            ->setCanvas('https://l.vemcount.com/login')
            ->setResolution('1920x1080')
            ->shoot()
        ;

        $result = file_exists('/screenshots/' . $filename);

        if ($result) {
            unlink('/screenshots/' . $filename);
        }

        $this->assertTrue($result);
    }

    /**
     * @test
     */
    public function a_error_happens()
    {
        $this->expectException(PuppeteerException::class);

        $this->puppeteer
            ->setScreenshotPath('/my/non/existing/path')
            ->setScreenshotType('full_screenshot_series')
            ->setCanvas('https://l.vemcount.com/login')
            ->setResolution('1920x1080')
            ->shoot()
        ;
    }
}
